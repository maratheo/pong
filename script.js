var canvasWidth = 600;
var canvasHeight = 350;
var p1Score = 0;
var p2Score = 0;

var ballX = canvasWidth/4;
var ballY = canvasHeight/2;
var ballWidth = 10;
var ballHeight = 10;
var ballSpeedX = 300; // px per second
var ballSpeedY = 300; // px per second

var p1PaddleWidth = 10;
var p1PaddleHeight = 50;
var p1PaddleX = 10;
var p1PaddleY = canvasHeight / 2;
var p1PaddleSpeed = 0;
var p2PaddleWidth = 10;
var p2PaddleHeight = 50;
var p2PaddleX = canvasWidth - p2PaddleWidth - 10;
var p2PaddleY = canvasHeight / 2;
var p2PaddleSpeed = 0;
var paddleSpeed = 300; // px per second
var paddleCollisionAudio = document.getElementById("paddle-collision");

var lastUpdateTime = Date.now();
var isPaused = true;

var canvas = document.getElementById("gameCanvas");
canvas.width = canvasWidth;
canvas.height = canvasHeight;
/** @type {CanvasRenderingContext2D} */
var ctx = canvas.getContext("2d");

if (!Math.sign) Math.sign = function(x) {
    if (x >= 0) {
        return 1;
    } else {
        return -1;
    }
}

document.addEventListener('keydown', keyDown);
document.addEventListener('keyup', keyUp);
function keyUp(event) {
    if (event.code == "ArrowUp" || event.code == "ArrowDown") {
        p2PaddleSpeed = 0;
    }

    if (event.code == "KeyW" || event.code == "KeyS") {
        p1PaddleSpeed = 0;
    }
}
function keyDown(event) {
    if (event.code == "ArrowUp") {
        p2PaddleSpeed = paddleSpeed * -1;
    } else if (event.code == "ArrowDown") {
        p2PaddleSpeed = paddleSpeed;
    }

    if (event.code == "KeyW") {
        p1PaddleSpeed = paddleSpeed * -1;
    } else if (event.code == "KeyS") {
        p1PaddleSpeed = paddleSpeed;
    }

    if (event.code == "KeyP") {
        if (isPaused) {
            resume();
        } else {
            pause();
        }
    }
}

function draw() {
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    
    // Draw player scores
    var fontSize = canvasHeight / 6;
    ctx.font = fontSize + "px Arial";
    ctx.fillStyle = "white";
    ctx.textAlign = "center";
    ctx.fillText(p1Score, canvas.width / 4, fontSize / 2 + fontSize / 3);
    ctx.fillText(p2Score, canvas.width * 3 / 4, fontSize / 2 + fontSize / 3);

    // Draw p1 paddle
    ctx.fillStyle = "red";
    ctx.fillRect(p1PaddleX, p1PaddleY, p1PaddleWidth, p1PaddleHeight);

    // Draw p2 paddle
    ctx.fillStyle = "blue";
    ctx.fillRect(p2PaddleX, p2PaddleY, p2PaddleWidth, p2PaddleHeight);

    // Draw net
    ctx.strokeStyle = "white";
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.setLineDash([3, 7]);
    ctx.moveTo(canvasWidth/2, 0);
    ctx.lineTo(canvasWidth/2, canvasHeight);
    ctx.stroke();

    // Draw ball
    ctx.fillStyle = "white";
    ctx.fillRect(ballX, ballY, ballWidth, ballHeight);
}

function getRandomNumber(min, max) {
    return Math.random() * (max - min) + min;
}

function update() {
    var currentTime = Date.now();
    var secondsElapsed = (currentTime - lastUpdateTime)/1000;
    secondsElapsed = Math.min(secondsElapsed, 0.1);
   
    // Update ball coordinates
    ballX = ballX + ballSpeedX * secondsElapsed;
    ballY = ballY + ballSpeedY * secondsElapsed;
    
    // Update paddle coordinates
    p1PaddleY = p1PaddleY + p1PaddleSpeed * secondsElapsed;
    p2PaddleY = p2PaddleY + p2PaddleSpeed * secondsElapsed;

    // Check side walls collisions
    if (ballX <= 0) {
        ballX = canvasWidth/2;
        ballY = canvasHeight/2;
        p2Score = p2Score + 1;
        ballSpeedY = 100 * (Math.random() < 0.5 ? -1 : 1);
    } else if (ballX + ballWidth >= canvasWidth) {
        ballX = canvasWidth/2;
        ballY = canvasHeight/2;
        p1Score = p1Score + 1;
        ballSpeedY = 100 * (Math.random() < 0.5 ? -1 : 1);
    }

    // Check wall collisions
    if (ballY <= 0) {
        ballSpeedY = Math.abs(ballSpeedY);
    } else if (ballY + ballHeight >= canvasHeight) {
        ballSpeedY = Math.abs(ballSpeedY) * -1;
    }

    if ((ballX + ballWidth >= canvasWidth) || (ballX <= 0)) {
        ballSpeedX = ballSpeedX * -1;
    }
    lastUpdateTime = Date.now();

    // Check paddle collisions with ball
    if (Math.abs(ballX - (p1PaddleX + p1PaddleWidth)) <= 5 && 
        (ballY + ballHeight >= p1PaddleY) &&
        (ballY <= p1PaddleY + p1PaddleHeight)) {
        ballSpeedX = Math.abs(ballSpeedX);
        paddleCollisionAudio.play();
        ballSpeedY = getRandomNumber(300, 450) * Math.sign(ballSpeedY);
        if (Math.random() < 0.1) {
            ballSpeedY = ballSpeedY * -1;
        }
        console.log(ballSpeedY);
    }

    if (Math.abs((ballX + ballWidth) - p2PaddleX) <= 5 && 
        (ballY + ballHeight >= p2PaddleY) &&
        (ballY <= p2PaddleY + p2PaddleHeight)) {
        ballSpeedX = Math.abs(ballSpeedX) * -1;
        paddleCollisionAudio.play();
        ballSpeedY = getRandomNumber(300, 450) * Math.sign(ballSpeedY);
        if (Math.random() < 0.1) {
            ballSpeedY = ballSpeedY * -1;
        }
        console.log(ballSpeedY);
    }

    // Check paddle collisions with walls
    if (p2PaddleY <= 0) {
        p2PaddleSpeed = 0;
        p2PaddleY = 0;
    } else if (p2PaddleY + p2PaddleHeight >= canvasHeight) {
        p2PaddleSpeed = 0;
        p2PaddleY = canvasHeight - p2PaddleHeight;
    }

    if (p1PaddleY <= 0) {
        p1PaddleSpeed = 0;
        p1PaddleY = 0;
    } else if (p1PaddleY + p1PaddleHeight >= canvasHeight) {
        p1PaddleSpeed = 0;
        p1PaddleY = canvasHeight - p1PaddleHeight;
    }
}

function gameLoop() {
    update();
    draw();
    if (!isPaused) {
        window.requestAnimationFrame(gameLoop);
    }
}

function pause() {
    isPaused = true;
}

function resume() {
    isPaused = false;
    window.requestAnimationFrame(gameLoop);
}

draw();